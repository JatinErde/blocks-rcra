README
-------------
Author 
	Javier Ludeña Montes

-------------- 
Incrementally solve a Unblock problem using languages C and Clingo.  

Given the board below, find a solution consisting of successive moves such that only block named as 'goal' remains in the position goal (b=4).


...a..
...a..
bb.a..
......
c.ddd.
c..ee.
b=4

example calls:
--------------
make
./unblock level1
./unblock level5 > ficheiro.txt



Notes
-----

Its necessary for Clingo to be installed in /usr/local/bin/

If it doesn't work you can uncomment last line in unblock.c and use clingo ver
which is in the same carpet (only for OS X).

// execl("./clingo", "clingo", "result.lp", NULL);