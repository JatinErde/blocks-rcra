CC=gcc
CFLAGS=-Wall -g

PROGS= unblock

all: $(PROGS)

% : %.c
	$(CC) $(CFLAGS) -o $@ $<

clean:
	rm -f $(PROGS) *.o *~
