/*
Authors.
        Xosé Estévez Lerez
        Javier Ludeña Montes

*/

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

struct block {
  char name;
  int row;
  int col;
  int size;
  int isRow; // 0 si es fila, 1 si es columna,
  struct block *next;
};
struct block *list = {NULL};

struct goal {
  char name;
  int row;
};
struct goal g;

/*funciones list bloque*/

struct block *createBlock(struct block *new, char name, int row, int col,
                          int isRow) {
  new = (struct block *)malloc(sizeof(struct block));

  new->name = name;
  new->row = row;
  new->size = 1; /*incializo tamaño 1*/
  new->row = row;
  new->col = col;
  new->isRow = isRow;

  return new;
}

struct block *blockSearch(char name) {
  struct block *p;
  p = list;

  while (p != NULL) {
    if (name == p->name)
      return p;
    p = p->next;
  }
  return NULL;
}

void blockAdd(char name, int isRow, int row, int col) {

  struct block *aux = {NULL};
  struct block *new = {NULL};
  new = createBlock(new, name, row, col, isRow);

  if (new == NULL)
    printf("No hay memoria ya en la lista\n");
  else {

    aux = list;
    if (list == NULL) {
      list = new;
    } else {
      while (aux->next != NULL) {
        aux = aux->next;
      }
      aux->next = new;
    }
  }
}

void blockModify(struct block *p) { p->size++; }

static void help() {
  printf(
      "Help: unblock\n"
      "resuelve niveles del unblock\n"
      "Opciones:\n"
      "Tienes que introducirle un nivel válido que este en la carpeta levels.\n"
      "Ejemplo: ./unblock level1\n\n");
  exit(0);
}

int printBlocks() {
  struct block *aux; /* recorremos la lista usando este puntero */
  aux = list;

  while (aux != NULL) {
    printf("Block: %c\n", aux->name);
    printf("Size: %d\n", aux->size);
    printf("Col: %d and Row: %d\n", aux->col, aux->row);
    if (aux->isRow == 0) {
      printf("Es una fila\n\n");
    } else
      printf("Es una columna\n\n");
    aux = aux->next;
  }

  //  printf("La meta es  %c en columna %d\n", g.name, g.row);

  return 0;
}

// Fin funciones lista block

// Funcion que lee el nivel
char *readlevels(int argc, char **argv) {

  if (argc == 1) {
    help();
    exit(0);
  }

  char *aux = malloc(100 * sizeof(char));
  char *levels = "levels/";
  char *target = malloc(20 * sizeof(char));

  strcpy(aux, argv[1]);
  strcpy(target, levels);
  strcat(target, aux);
  strcat(target, ".txt");

  free(aux);

  return target;
}

void readFile(FILE *f) {
  char cadena[8];
  int row = 0;

  while (fgets(cadena, 8, f) != NULL) {

    if (row == 6) {
      g.name = cadena[0];
      g.row = cadena[2] - 48;
      break;
    }

    for (int col = 0; col < sizeof(cadena); col++) {
      char c = cadena[col];
      if ((c != '.') && (c != '\n') && col <= 6) {
        struct block *p = blockSearch(c);

        if (p == NULL) {
          int isRow = 1;
          if (c == cadena[col + 1])
            isRow = 0;

          blockAdd(c, isRow, row, col);
        } else
          blockModify(p);
      }
    }
    row++;
  }
}

int printRules(FILE *f) {
  FILE *frules = fopen("ASP/rules.lp", "r");
  char c;
  if (!(frules) || !(f)) {
    perror("Error de apertura de ficheros");
    exit(EXIT_FAILURE);
  }

  while ((c = fgetc(frules)) != EOF && !ferror(f) && !ferror(frules))
    fputc(c, f);

  if (ferror(frules) || ferror(f))
    return 1;

  fclose(frules);
  return 0;
}

void printincmode(FILE *f) {

  char *cadena = malloc(100 * sizeof(char));

  strcpy(cadena, "#include <incmode>.\n");
  fputs(cadena, f);
  strcpy(cadena, "#program base.\n\n");

  fputs(cadena, f);
  strcpy(cadena, "row(0..5).\n");
  fputs(cadena, f);
  strcpy(cadena, "col(0..5).\n");
  fputs(cadena, f);

  strcpy(cadena, "inc(1;-1).\n");
  fputs(cadena, f);

  free(cadena);
}

void printasp_block(FILE *f) {

  struct block *aux; /* recorremos la lista usando este puntero */
  aux = list;

  char *cadena = malloc(100 * sizeof(char));

  strcpy(cadena, "block(");

  while (aux != NULL) {
    strcat(cadena, &aux->name);
    if (aux->next != NULL)
      strcat(cadena, ";");
    aux = aux->next;
  }

  strcat(cadena, ").\n\n");
  fputs(cadena, f);
  free(cadena);
}
void printasp(FILE *f) {

  struct block *aux; /* recorremos la lista usando este puntero */
  aux = list;
  char buffer[20];
  while (aux != NULL) {

    char *row = malloc(20 * sizeof(char));
    char *col = malloc(20 * sizeof(char));
    char *tam = malloc(20 * sizeof(char));

    strcpy(row, "row(");
    strcpy(col, "col(");
    strcpy(tam, "tam(");

    sprintf(buffer, "%d", aux->row);
    strcat(row, &aux->name);
    strcat(row, ",");
    strcat(row, buffer);

    if (aux->isRow == 1)
      strcat(row, ",0).\n");
    else
      strcat(row, ").\n");
    fputs(row, f);

    sprintf(buffer, "%d", aux->col);
    strcat(col, &aux->name);
    strcat(col, ",");
    strcat(col, buffer);

    if (aux->isRow == 0)
      strcat(col, ",0).\n");
    else
      strcat(col, ").\n");
    fputs(col, f);

    sprintf(buffer, "%d", aux->size);
    strcat(tam, &aux->name);
    strcat(tam, ",");
    strcat(tam, buffer);
    strcat(tam, ").\n\n");
    fputs(tam, f);

    free(row);
    free(col);
    free(tam);
    aux = aux->next;
  }
}
void printasap_goal(FILE *f) {

  char buffer[20];
  char *cadena = malloc(100 * sizeof(char));

  sprintf(buffer, "%d", g.row);
  strcpy(cadena, "goal(t) :- col(");
  strcat(cadena, &g.name);
  strcat(cadena, ",");
  strcat(cadena, buffer);
  strcat(cadena, ",t).\n");
  fputs(cadena, f);

  char *moves = "\n\n#show move/3.\n";
  fputs(moves, f);
  free(cadena);
}

void printBase(FILE *f) {
  printincmode(f);
  printasp_block(f);
  printasp(f);
}

void writeFile(FILE *f) {

  printBase(f);
  printRules(f);
  printasap_goal(f);
}

int exit_handler() {
  /*liberamos memoria */

  struct block *aux = list, *p;

  if (list != NULL) {
    while (aux != NULL) {
      p = aux;
      aux = aux->next;
      free(p);
    }
  }
  return 0;
}
int main(int argc, char *argv[]) {

  char *fichero = readlevels(argc, argv);
  FILE *resultFile = fopen("result.lp", "w");
  FILE *f = fopen(fichero, "r");

  printf("unblock v0.1\n");

  if (f == NULL) {
    perror("Error al abrir el nivel");
    help();
    return -1;
  }

  readFile(f);
  // printBlocks();
  writeFile(resultFile);

  exit_handler();
  free(fichero);
  fclose(f);
  fclose(resultFile);

  execl("/usr/local/bin/clingo", "clingo", "result.lp", NULL);
  // execl("./clingo", "clingo", "result.lp", NULL);
}
